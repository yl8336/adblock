# adblock
## 规则订阅
### 订阅
| 规则名称 | 介绍 | 订阅链接 |
| :-- | :-- | :-- |
| **Easylist** | 提供Easylist规则合并、精简、补充，不定期更新 | [订阅链接](https://gitea.com/lonely_dog/adblock/raw/branch/master/EasylistCombine) | 
| **混合规则** | 合并于各种知名的Adblock规则，不定期更新 | [订阅链接](https://gitea.com/lonely_dog/adblock/raw/branch/master/adblock) |
| **混合规则(精简版)** | 适用于via，可与其他规则合用，不定期更新 | [订阅链接](https://gitea.com/lonely_dog/adblock/raw/branch/master/adblock_lite) |     
| **MIUI浏览器规则** | 取自 ``MIUI浏览器12.0.1版本`` + ``Easylist规则(精简) `` ，不定期更新 | [订阅链接](https://gitea.com/lonely_dog/adblock/raw/branch/master/miuiadblock) |

- ## 注意
> #### ①如果订阅了 **[混合规则](https://gitea.com/lonely_dog/adblock/raw/branch/master/adblock)** 就不要订阅其他规则，否则有可能会冲突，导致规则失效。
> #### ②其他合并的规则随意
> ### ③ 使用[Via浏览器](https://www.coolapk.com/apk/mark.via) 或者[Rains浏览器](https://www.coolapk.com/apk/com.rainsee.create) 请务必使用 **[混合规则精简版](https://gitea.com/lonely_dog/adblock/raw/branch/master/adblock_lite)**
